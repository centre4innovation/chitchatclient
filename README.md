# chitchatclient.js #

A pure JavaScript chat widget. There are no external dependencies.

There are two client implementations provide.

- `chitchatclient.polyfill.js` to ensure some JS functionality.
- `chitchatclient.core.js` that is renders the basic interface.
- `chitchatclient.rich.js` that adds extra functionality to the core, tailored to connecting to a ChitChat server instance. It handles rendering of images, buttons, speech recognition and synthesis.

![Screenshot](https://bitbucket.org/centre4innovation/chitchatclient/raw/4d7bad93f5f30ca8fb83f6c41c895fab09cccdb6/src/chitchatclient.png "Screenshot")


## Getting started ##

For the basic functionality, just include `chitchatclient.core.css` and `chitchatclient.polyfill.js`, `chitchatclient.core.js`, and turn a container element into a chat box.

```html
<link rel="stylesheet" type="text/css" href="chitchatclient.core.css">
<script src="chitchatclient.polyfill.js"></script>
<script src="chitchatclient.core.js"></script>

<script> 
    new ChitChatClient(document.getElementById('mydiv'));
</script>
```

You can provide some options when constructing the ChitChatClient or providing those 
properties as URL parameters. In this way the client can be modified by the website embedding the chat client.

So, if you want to change the title for example, you can also call

```
index-rich.html?title=My%20title
```

All options with their defaults for the core client: 

```javascript
new ChitChatClient(document.getElementById('mydiv'), {
    lang: 'en',                 // Language code used for UI texts
    showLeftUser: true,         // Show bot icon or not
    showRightUser: false,       // Show user icon or not
    scrollToLastMessage: true,  // Auto-scroll to bottom
    onSend: (obj) => {},        // Callback when sending. Object argument with text, timestamp, user info.
    rightUserInitials: "ME",    // What text to show in right user icon (if shown)
    leftUserInitials: "YOU",    // What text to show in left bot icon (if shown)
    leftMessageClass: "animate fadeInLeft",   // Message appearance effect
    rightMessageClass: "animate fadeInRight", // Message appearance effect
    forceTurnTaking: false,     // Allow sending multiple message by user, before getting an answer back
    defaultDelay: 500           // Add extra time before showing a reply from the bot
});
```

For the rich client, also include `chitchatclient.components.css` and `chitchatclient.rich.js` files.
This client has a more rich feature set and assumes you connect it to a chitchat server.

Additional features include:
- Rendering images, buttons and checkboxes
- Speech recognition input
- (Triggering) a welcome message

All previous options apply, and additional properties can be set ()

```javascript
new RichChitChatClient(document.getElementById('chitchatclient'), {
    lang: 'en',                  // Language code used for UI texts and speech recognition
    chitChatHost: '',            // ChitChat server base url
    chitChatEndpoint: undefined, // url path to /reply
    title: 'ChitChat',           // Title text
    description: undefined,      // Subtitle text
    userId: 'user-' + randomString(8), // User identifier
    autoSendAfterSpeech: true,   // Need to push send button or not
    script: undefined,           // Chitchat bot identifier
    welcome: undefined,          // Initial message shown
    given: undefined,            // Implicit text send by user at start
    stylePath: '',               // Path prefix for css styles (e.g. '/static')
    style: undefined             // Part of css style file 'green' (refers to 'chitchatclient.style.green.css' 
});
```

## Compatibility ##
To enable support for older browsers (no ES6), use Babel (https://babeljs.io/setup#installation)

Make sure to install:
`npm install babel-core babel-preset-env --save-dev`

Then run `npm run build`, and find the converted script in `lib/`

For css, consider:
```
npm install postcss-cli
npm install postcss-preset-env  --save-dev
```

Run
```
postcss --use autoprefixer -c options.json -o main.css css/*.css
```

## Credits ##
Arvid Halma (HumanityX, Centre for Innovation, Leiden University)

## License ##
Copyright 2018 Centre for Innovation, Leiden University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.