/**
 * Pure JS chat client widget.
 * @author Arvid Halma, Center for Innovation, Leiden University
 */
class ChitChatClient {



    /**
     * Create and show a new chat client.
     * @param parent {HTMLElement} container
     * @param options {object} with fields to customize the client.
     * {
            lang: 'en',                 // Language code used for UI texts
            showLeftUser: true,         // Show bot icon or not
            showRightUser: false,       // Show user icon or not
            scrollToLastMessage: true,  // Auto-scroll to bottom
            onSend: (obj) => {},        // Callback when sending. Object argument with text, timestamp, user info.
            rightUserInitials: "ME",    // What text to show in right user icon (if shown)
            leftUserInitials: "BOT",    // What text to show in left bot icon (if shown)
            leftMessageClass: "fade-msg",   // Message appearance effect
            rightMessageClass: "fade-msg",  // Message appearance effect
            forceTurnTaking: false,     // Allow sending multiple message by user, before getting an answer back
            defaultDelay: 500           // Add extra time before showing a reply from the bot
        }
     */
    constructor(parent, options) {
        // translated UI labels
        this.i18n = {
            'ar' : {
                'typeHerePlaceholder': 'أكتب هنا...'
            },
            'fr' : {
                'typeHerePlaceholder': 'Écrivez ici...'
            },
            'en' : {
                'typeHerePlaceholder': 'Type here...'
            },
            'es' : {
                'typeHerePlaceholder': 'Escriba aquí...'
            },
            'nl' : {
                'typeHerePlaceholder': 'Typ hier...'
            }
        }

        const self = this
        this.parent = parent
        this.parent.classList.add('chitchatclient')
        this.stack = []
        this.options = Object.assign({
            lang: 'en',                 // Language code used for UI texts
            showLeftUser: true,         // Show bot icon or not
            showRightUser: false,       // Show user icon or not
            scrollToLastMessage: true,  // Auto-scroll to bottom
            onSend: (obj) => {},        // Callback when sending. Object argument with text, timestamp, user info.
            rightUserInitials: "ME",    // What text to show in right user icon (if shown)
            leftUserInitials: "BOT",    // What text to show in left bot icon (if shown)
            leftMessageClass: "fade-msg",   // Message appearance effect
            rightMessageClass: "fade-msg",  // Message appearance effect
            forceTurnTaking: false,     // Allow sending multiple message by user, before getting an answer back
            defaultDelay: 500           // Add extra time before showing a reply from the bot
        }, options)

        if(!this.options.showRightUser){
            parent.classList.add('norightuser')
        }

        if(!this.options.showLeftUser){
            parent.classList.add('noleftuser')
        }

        // internal state
        this.leftRightState = 0 // -1 = left, 1 = right
        this.lastMessageContainer = undefined

        // build dom
        this.messagesDiv = document.createElement('div')
        this.messagesDiv.classList.add('messages')
        this.parent.appendChild(this.messagesDiv)

        this.sendDiv = document.createElement('div')
        this.sendDiv.classList.add('send')
        this.sendDiv.innerHTML += `
            <div style="display: table-cell;vertical-align: middle;width: 100%;">
            <input type="text" placeholder="${this.getText('typeHerePlaceholder')}">
            
            <a class="micBtn">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="20px" height="20px">
                <path style="text-transform:none;block-progression:tb;isolation:auto;mix-blend-mode:normal" d="M 7.5 1 C 6.1272727 1 5 2.1272727 5 3.5 L 5 7.5 C 5 8.8727273 6.1272727 10 7.5 10 C 8.8727273 10 10 8.8727273 10 7.5 L 10 3.5 C 10 2.1272727 8.8727273 1 7.5 1 z M 7.5 2 C 8.3272727 2 9 2.6727273 9 3.5 L 9 7.5 C 9 8.3272727 8.3272727 9 7.5 9 C 6.6727273 9 6 8.3272727 6 7.5 L 6 3.5 C 6 2.6727273 6.6727273 2 7.5 2 z M 3 7 L 3 7.5 C 3 9.8014591 4.7630368 11.694903 7 11.949219 L 7 14 L 5 14 L 5 15 L 10 15 L 10 14 L 8 14 L 8 11.949219 C 10.236963 11.694903 12 9.8014591 12 7.5 L 12 7 L 11 7 L 11 7.5 C 11 9.4272727 9.4272727 11 7.5 11 C 5.5727273 11 4 9.4272727 4 7.5 L 4 7 L 3 7 z" overflow="visible"  fill="#FFFFFF"/>
                </svg>
            </a>
            </div> 
            <div style="display: table-cell; width: 50px; text-align: right;">
            <a class="sendBtn">
                <svg xmlns="http://www.w3.org/2000/svg"  version="1.1" x="0px" y="0px" viewBox="0 0 486.736 486.736" style="enable-background:new 0 0 486.736 486.736;" xml:space="preserve" width="20px" height="20px">
                <g><path d="M481.883,61.238l-474.3,171.4c-8.8,3.2-10.3,15-2.6,20.2l70.9,48.4l321.8-169.7l-272.4,203.4v82.4c0,5.6,6.3,9,11,5.9   l60-39.8l59.1,40.3c5.4,3.7,12.8,2.1,16.3-3.5l214.5-353.7C487.983,63.638,485.083,60.038,481.883,61.238z" fill="#FFFFFF"/></g>
                </svg>
            </a>
            </div>`
        this.parent.appendChild(this.sendDiv)


        let $input = parent.querySelector('.send input')

        function onSend() {
            const msg = $input.value
            if (msg.length === 0) {
                return
            }
            let timestamp = new Date()
            self.addMessageRight(msg, self.options.rightUserInitials, 0)
            $input.value = ''
            self.options.onSend({text:msg, timestamp:timestamp, from:self.options.rightUserInitials})
        }

        parent.querySelector('.sendBtn').addEventListener('click', onSend)

        $input.addEventListener("keydown", event => {
            if (event.key === "Enter" || event.which === 13 || event.keyCode === 13) {
                onSend()
                return false
            }
            return true
        })

        // render update timer
        window.setInterval(function(){
            let now = Date.now();

            while(self.stack.length > 0) {
                let msg = self.stack[0];
                if (msg.timestamp <= now) {
                    self.stack.shift();
                    if(msg.text) {
                        if (msg.place === 'left') {
                            self.renderMessageLeft(msg.text, msg.from, msg.timestamp.toTimeString().substring(0, 5))
                        } else if (msg.place === 'right') {
                            self.renderMessageRight(msg.text, msg.from, msg.timestamp.toTimeString().substring(0, 5))
                        } else if (msg.place === 'info') {
                            self.renderInfo(msg.text)
                        }
                    }
                } else {
                    break
                }
            }
        }, 100);

    }

    getText(key){
        if(this.options.lang in this.i18n){
            return this.i18n[this.options.lang][key] || key;
        } else {
            return key;
        }
    }

    addHistoryMessageLeft(msg, user, timestr){
        this.renderMessageLeft(msg, user, timestr, "noAnimation")
    }

    /**
     * Add a message, that the other party sends, on the left-hand side.
     * @param msg the content
     * @param user the user name
     * @param delay milliseconds after the last message, before it appears
     * @returns {{from: *, text: *, place: string, timestamp: Date}}
     */
    addMessageLeft(msg, user, delay) {
        let t = this.stack.length > 0 ? this.stack[this.stack.length - 1].timestamp : new Date();
        const msgObj = {text:msg, timestamp: new Date(t.getTime() + (typeof delay === 'undefined' ? this.options.defaultDelay * 1: delay)), from:user, place: 'left'};
        this.stack.push(msgObj)
        return msgObj;
    }

    addWait(delay) {
        let t = this.stack.length > 0 ? this.stack[this.stack.length - 1].timestamp : new Date();
        this.stack.push({timestamp: new Date(t.getTime() + (typeof delay === 'undefined' ? this.options.defaultDelay * 1: delay)), place: 'left'})
    }

    addHistoryMessageRight(msg, user, timestr){
        this.renderMessageRight(msg, user, timestr, "noAnimation")
    }

    /**
     * Add a message, that the user sends, on the right-hand side.
     * @param msg the content
     * @param user the user name
     * @param delay milliseconds after the last message, before it appears
     * @returns {{from: *, text: *, place: string, timestamp: Date}}
     */
    addMessageRight(msg, user, delay) {
        let t = new Date();
        if(this.options.rightUserInitials !== 'serverId') {
            user = this.options.rightUserInitials;
        }

        if(this.options.forceTurnTaking){
            if(this.stack.length > 0) {
                t = this.stack[this.stack.length - 1].timestamp
            }
            const msgObj = {text:msg, timestamp: new Date(t.getTime() + (typeof delay === 'undefined' ? this.options.defaultDelay * 1: delay)), from:user, place: 'right'};
            this.stack.push(msgObj)
            return msgObj;
        } else {
            const msgObj = {text:msg, timestamp: new Date(t.getTime() + (typeof delay === 'undefined' ? this.options.defaultDelay * 1: delay)), from:user, place: 'right'};
            this.stack.unshift(msgObj)
            return msgObj
        }
    }

    /**
     * Render an incoming message (message received).
     * @param msg {string} html content
     * @param user {string} user initials (space for 2-3 characters).
     *   If undefined, the default this.options.leftUserInitials is used.
     * @param timestr {string} an indication of when this message arrived.
     *   If it is undefined the current time will be used.
     * @param animationClass override css transition class
     */
    renderMessageLeft(msg, user, timestr, animationClass) {
            if(this.options.leftUserInitials !== 'serverId') {
                user = this.options.leftUserInitials;
            }

            const userHtml = this.options.showLeftUser ? `<div class="user">${user}</div>` : ''
            const arrowHtml = this.leftRightState !== -1 ? `<div class="arrow"><svg height="10" width="20"><polygon points="0,0 20,0 10,10" style="stroke:none;stroke-width:0" /></svg></div>` : ''
            if(this.leftRightState !== -1){
                // last message not from left user
                this.lastMessageContainer = document.createElement('div')
                this.lastMessageContainer.classList.add('left')
                this.lastMessageContainer.innerHTML += userHtml
                this.lastMessageContainer.innerHTML += `<div class="msg ${animationClass || this.options.leftMessageClass}">${arrowHtml} ${msg}<div class="timestamp">${timestr || ''}</div></div>`
                this.messagesDiv.appendChild(this.lastMessageContainer)
            } else {
                // append message from left user
                const msgDiv = document.createElement('div')
                msgDiv.classList.add('msg', (animationClass || this.options.leftMessageClass))
                msgDiv.innerHTML += `${msg}<div class="timestamp">${timestr || ''}</div>`
                this.lastMessageContainer.appendChild(msgDiv);
            }
            if (this.options.scrollToLastMessage) {
                if (typeof this.messagesDiv.scrollTo != 'function') {
                    this.messagesDiv.scrollTop = this.messagesDiv.scrollHeight;
                } else {
                    this.messagesDiv.scrollTo({ "behavior": "smooth", "top": 1000000 });
                }

            }
            this.leftRightState = -1

    }

    /**
     * Render an outgoing message (message sent).
     * @param msg {string} html content
     * @param user {string} user initials (space for 2-3 characters).
     *   If undefined, the default this.options.rightUserInitials is used.
     * @param timestr {string} an indication of when this message was sent.
     *   If it is undefined the current time will be used.
     * @param animationClass override css transition class
     */
    renderMessageRight(msg, user, timestr, animationClass) {
            const userHtml = this.options.showRightUser? `<div class="user">${user || this.options.rightUserInitials}</div>` : ''
            const arrowHtml = this.leftRightState !== 1 ? `<div class="arrow"><svg height="10" width="20"><polygon points="0,0 20,0 10,10" style="stroke:none;stroke-width:0" /></svg></div>` : ''

            if(this.leftRightState !== 1){
                // last message not from right user
                this.lastMessageContainer = document.createElement('div')
                this.lastMessageContainer.classList.add('right')
                this.lastMessageContainer.innerHTML += userHtml
                this.lastMessageContainer.innerHTML += `<div class="msg ${animationClass || this.options.rightMessageClass}">${arrowHtml} ${msg}<div class="timestamp">${timestr || ''}</div></div>`
                this.messagesDiv.appendChild(this.lastMessageContainer)
            } else {
                // append message from right user
                const msgDiv = document.createElement('div')
                // msgDiv.classList.add('msg', (animationClass || this.options.rightMessageClass))
                msgDiv.classList.add('msg', (animationClass || this.options.rightMessageClass))
                msgDiv.innerHTML += `${msg}<div class="timestamp">${timestr || ''}</div>`;
                this.lastMessageContainer.appendChild(msgDiv);
            }

        if (this.options.scrollToLastMessage) {
            if (typeof this.messagesDiv.scrollTo != 'function') {
                this.messagesDiv.scrollTop = this.messagesDiv.scrollHeight;
            } else {
                this.messagesDiv.scrollTo({ "behavior": "smooth", "top": 1000000 });
            }

        }

            this.leftRightState = 1
    }

    /**
     * A centered notification
     * @param msg {string} html message.
     */
    addHistoryInfo(msg) {
        this.renderInfo(msg);
    }

    /**
     * A centered notification
     * @param msg {string} html message.
     */
    addInfo(msg) {
        const msgObj = {text:msg, timestamp: new Date(), place: 'info'};
        this.stack.push(msgObj)
        return msgObj;
    }

    /**
     * A centered notification
     * @param msg {string} html message.
     */
    renderInfo(msg) {
        const infoElm = document.createElement('div')
        infoElm.classList.add('info')
        infoElm.innerHTML = msg
        this.messagesDiv.appendChild(infoElm)
        this.leftRightState = 0
    }

    /**
     * Remove all messages
     */
    clear(){
        this.messagesDiv.innerHTML = ""
    }

    /**
     * Set hook for when a message was send by the user.
     *
     * @param f {Function} to be called. It receives an object as argument with the following fields.
     * {text:msg, timestamp:timestamp, from:self.options.rightUserInitials}
     */
    onSend(f){
        this.options.onSend = f
    }


}