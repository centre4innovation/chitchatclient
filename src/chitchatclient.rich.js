/**
 * Pure JS chat client widget, with predefined behavior to connect to a ChitChat server.
 * @author Arvid Halma, Center for Innovation, Leiden University
 */
class RichChitChatClient {

    /**
     * Create and show a new rich chat client.
     * @param parent {HTMLElement} container
     * @param options {object} with fields to customize the client. In addition to the core client properties,
     * you can specify:
     * {
            lang: 'en',                  // Language code used for UI texts and speech recognition
            chitChatHost: '',            // ChitChat server base url
            chitChatEndpoint: undefined, // url path to /reply
            title: 'ChitChat',           // Title text
            description: undefined,      // Subtitle text
            userId: 'user-' + randomString(8), // User identifier
            autoSendAfterSpeech: true,   // Need to push send button or not
            script: undefined,           // Chitchat bot identifier
            welcome: undefined,          // Initial message shown
            given: undefined,            // Implicit text send by user at start
            stylePath: '',               // Path prefix for css styles (e.g. '/static')
            style: undefined             // Part of css style file 'green' (refers to 'chitchatclient.style.green.css'
        }
     */
    constructor(parent, options) {
        const self = this;
        // override
        this.options = Object.assign({
            lang: 'en',                  // Language code used for UI texts and speech recognition
            chitChatHost: '',            // ChitChat server base url
            chitChatEndpoint: undefined, // url path to /reply
            title: 'ChitChat',           // Title text
            description: undefined,      // Subtitle text
            userId: 'user-' + randomString(8), // User identifier
            botId: 'chitchat',           // Bot identifier
            autoSendAfterSpeech: true,   // Need to push send button or not
            script: undefined,           // Chitchat bot identifier
            welcome: undefined,          // Initial message shown
            given: undefined,            // Implicit text send by user at start
            stylePath: '',               // Path prefix for css styles (e.g. '/static')
            style: undefined,            // Part of css style file 'green' (refers to 'chitchatclient.style.green.css'
            history: true                // Whether messages are stored and recovered from local storage
        }, options);

        updateWithQueryParams(this.options);
        
        usestyle(this.options)

        const info = {};
        info.userAgent = navigator.userAgent;
        info.lang = this.options.lang;

        // Get language setting
        const lang = this.options.lang;
        function speechLangString(lang){
            speechLang = 'en-US';
            if (lang) {
                if (lang.length === 2 && lang !== 'en') {
                    speechLang = lang.toLowerCase() + '-' + lang.toUpperCase()
                } else {
                    speechLang = lang;
                }
            }
            return speechLang;
        }

        // Update page title
        const title = this.options.title;
        const titleElt = document.getElementById('chitchatclient-title');
        if(title) {
            document.title = title;
            if(titleElt) {
                titleElt.innerText = title;
                titleElt.style.display = 'inline';
            }
        } else {
            // hide title
            if(titleElt) {
                titleElt.style.display = 'none';
            }
        }

        // Update description
        const description = this.options.description;
        const descriptionElt = document.getElementById('chitchatclient-description');
        if(descriptionElt)
        {
            if (description) {
                descriptionElt.innerText = description;
                descriptionElt.style.display = 'inline';
            } else {
                // hide title
                descriptionElt.style.display = 'none';
            }
        }

        // Update logo
        const logo = this.options.logo;
        if(logo) {
            let logoElt = document.getElementById('chitchatclient-logo');
            if(logoElt){
                logoElt.src = this.options.logo;
            }
        }

        // Create core client
        let chitChatClient = new ChitChatClient(parent, this.options);
        this.chitChatClient = chitChatClient;

        document.querySelectorAll('.chitchat-reset').forEach(e => e.addEventListener('click', function(){
            self.clear();
        }));

        // Setup speech synthesis
        let speechSynth;
        let speechLang;
        if ("speechSynthesis" in window) {
            info.speechSynthesis = 'enabled';
            speechSynth = window.speechSynthesis;
            speechLang = speechLangString(lang);
        } else {
            info.speechSynthesis = 'disabled';
        }

        // Setup speech recognition
        const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
        const micBtn = parent.querySelector('.micBtn');
        if (SpeechRecognition) {
            info.speechRecognition = 'enabled';
            micBtn.style.display = 'inline-flex';
            const micBtnColor = micBtn.style.backgroundColor;

            const recognition = new SpeechRecognition();
            recognition.lang = speechLangString(lang);

            micBtn.addEventListener('click', () => {
                recognition.start();
                micBtn.style.backgroundColor = '#f66e84';
                recognition.onresult = (event) => {
                    const speechToText = event.results[0][0].transcript;
                    const $input = parent.querySelector('.send input');
                    $input.value = speechToText;
                    micBtn.style.backgroundColor = micBtnColor;

                    if(self.options.autoSendAfterSpeech){
                        parent.querySelector('.sendBtn').click();
                    }
                }
            });
        } else {
            info.speechRecognition = 'disabled';
            micBtn.style.display = 'none';
        }


        function renderBotMessage(reply, historical) {
            let text = reply.text

            // render image
            text = text.replace(/\bIMAGE *\((.*?)\)/gm, '<img src="$1" style="display: inline;max-width:100%; width: auto; height: auto;"/>')

            // render buttons
            text = text.replace(/\bBUTTON? *\( *([^)]+) *, *(.*?) *\)/gm, `<button type="button" class="btn btn-success chatBtn" data-value="$2" ${historical ? 'disabled' : ''}>$1</button>`)
            text = text.replace(/\bBUTTONW? *\( *([^)]+) *, *(.*?) *\)/gm, `<button type="button" class="btn btn-success chatBtn chatBtnWide" data-value="$2" ${historical ? 'disabled' : ''}>$1</button>`)
            text = text.replace(/\bBUTTON? *\( *(.*?) *\)/gm, `<button type="button" class="btn btn-success chatBtn" data-value="$1" ${historical ? 'disabled' : ''}>$1</button>`)
            text = text.replace(/\bBUTTONW? *\( *(.*?) *\)/gm, `<button type="button" class="btn btn-success chatBtn chatBtnWide" data-value="$1" ${historical ? 'disabled' : ''}>$1</button>`)
            text = text.replace(/\bGEOBUTTON *\( *(.*?) *\)/gm, `<button type="button" class="btn btn-success chatBtn geoBtn" data-value="geoBtn" ${historical ? 'disabled' : ''}>$1</button>`)
            const textBefore = text;
            text = text.replace(/\bCHECKBOX *\( *([^)]+) *, *(.*?) *\)/gm, `<label class="checkBoxRow">$1 <input type="checkbox" value="$2" ${historical ? 'disabled' : ''}><span class="checkmark"></span></label>`)
            text = text.replace(/\bCHECKBOX *\( *(.*?) *\)/gm, `<label class="checkBoxRow">$1 <input type="checkbox" value="$1" ${historical ? 'disabled' : ''}><span class="checkmark"></span></label>`)
            if(textBefore !== text && !historical){
                // checkboxes detected
                text += '<button type="button" class="btn btn-success checkboxSendBtn">OK</button>'
            }

            // render image
            const yw = document.getElementById('chitchatclient').offsetWidth - 125;
            text = text.replace(/\bYOUTUBE *\((.*?)\)/gm, '<div class="youtube" style="width:'+yw+'px"><iframe src="https://www.youtube.com/embed/$1?version=3&amp;rel=0&amp;showinfo=0" frameborder="0" allow="encrypted-media" allowfullscreen></iframe></div>')

            // speech synthesis
            let speakPattern = /\bSPEAK *\((.*?)\)/gm;
            if (speechSynth && !historical) {
                // if speech supported
                let utterances;
                while ((utterances = speakPattern.exec(text)) !== null) {
                    const utterThis = new SpeechSynthesisUtterance(utterances[1]);
                    if (speechLang) {
                        utterThis.lang = speechLang;
                    }
                    speechSynth.speak(utterThis);
                }
            }

            // wait
            if(!historical) {
                let waitPattern = /\bWAIT *\((\d+)\)/gm;
                let waits;
                while ((waits = waitPattern.exec(text)) !== null) {
                    chitChatClient.addWait(waits[1] * 1);
                }
            }

            // remove special codes
            text = text.replace(/\b(SPEAK|WAIT) *\([^)]*\)/gm, '');

            // add text to panel
            return text;
        }

        let onSend = function (params) {
            if(params.text === 'chitchatclientinfo'){

                if(speechSynth) {
                    info.speechSynthesisLangs = speechSynth.getVoices().map(v => v.lang).join(', ');
                }
                chitChatClient.addMessageLeft('ChitChat client details...', '?')
                chitChatClient.addMessageLeft('User ID: ' + self.options.userId, '?');
                chitChatClient.addMessageLeft('Language code: ' + info.lang, '?');
                chitChatClient.addMessageLeft('Browser/user agent: ' + info.userAgent, '?');
                chitChatClient.addMessageLeft('Speech recognition: ' + info.speechRecognition, '?');
                chitChatClient.addMessageLeft('Speech synthesis: ' + info.speechSynthesis + ': ' + info.speechSynthesisLangs, '?');
                return
            }
            const formData = new FormData();
            formData.append("msg",
                JSON.stringify({
                    id: '' + Date.now(),
                    text: params.text,
                    senderId: self.options.userId,
                    recipientId: self.options.botId,
                    timestamp: params.timestamp
                }));


            let url;
            if(self.options.chitChatEndpoint){
                url = self.options.chitChatEndpoint
            } else if(self.options.script){
                url = self.options.chitChatHost + "/api/v1/channel/chitchat/mt/" + encodeURIComponent(self.options.script) + "/reply";
            } else {
                url = self.options.chitChatHost + "/api/v1/channel/chitchat/reply";
            }

            const xhr = new XMLHttpRequest();
            xhr.open('POST', url);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    const a = JSON.parse(xhr.responseText);
                    a.replies.forEach(r => self.addMessageLeft(renderBotMessage(r), self.options.leftUserInitials))
                }
                else if (xhr.status !== 200) {
                    chitChatClient.addInfo("<div style='color:#f06;font-style: italic'>The chatbot is not available...</div>")
                }
            };
            xhr.send(formData);
            self.updateHistory({text: params.text, timestamp: params.timestamp, from: self.options.userId, place: 'right'})
        };

        chitChatClient.onSend(onSend);

        // link button actions
        document.addEventListener('click',function(e){
            if(e.target && e.target.classList.contains('chatBtn')){
                let value = e.target.getAttribute('data-value');
                if(value === 'geoBtn'){
                    // share geolocation button
                    if ("geolocation" in navigator) {
                        /* geolocation is available */
                        navigator.geolocation.getCurrentPosition(function(position) {
                            const btnParams = {
                                id: '' + Date.now(),
                                text: 'latitude:'+position.coords.latitude+' longitude:'+position.coords.longitude,
                                senderId: self.options.userId,
                                recipientId: self.options.botId,
                                timestamp: new Date()
                            }

                            e.target.classList.add('chosen')
                            e.target.setAttribute('disabled', true)
                            getSiblings(e.target).forEach(btn => btn.setAttribute('disabled', true));
                            onSend(btnParams)
                        });
                    } else {
                        /* geolocation IS NOT available */
                        chitChatClient.addInfo("<div style='color:#f06;font-style: italic'>The geolocation is not available...</div>")
                        // normal button
                        const btnParams = {
                            id: '' + Date.now(),
                            text: 'unknownlocation',
                            senderId: self.options.userId,
                            recipientId: self.options.botId,
                            timestamp: new Date()
                        }
                        e.target.classList.add('chosen')
                        e.target.setAttribute('disabled', true)
                        getSiblings(e.target).forEach(btn => btn.setAttribute('disabled', true));
                        onSend(btnParams)
                    }

                } else {
                    // normal button
                    const btnParams = {
                        id: '' + Date.now(),
                        text: value,
                        senderId: self.options.userId,
                        recipientId: self.options.botId,
                        timestamp: new Date()
                    }
                    e.target.classList.add('chosen')
                    e.target.setAttribute('disabled', true)
                    getSiblings(e.target).forEach(btn => btn.setAttribute('disabled', true));
                    onSend(btnParams)
                }
            }
        })

        // link button actions
        document.addEventListener('click',function(e) {
            if (e.target && e.target.classList.contains('checkboxSendBtn')) {
                let checkedList = [...e.target.parentNode.querySelectorAll('input:checked')];
                let txt = checkedList.map(cb => cb.value).join(", ");
                txt = txt || 'NONECHECKED';
                const msgParams = {
                    id: '' + Date.now(),
                    text: txt,
                    senderId: self.options.userId,
                    recipientId: self.options.botId,
                    timestamp: new Date()
                }

                // submit button
                e.target.setAttribute('disabled', true)

                // options
                checkedList.forEach(cb => cb.classList.add('chosen'))
                e.target.parentNode.querySelectorAll('input').forEach(cb => cb.setAttribute('disabled', true));
                onSend(msgParams)
            }
        })

        // Show initial message
        if(this.options.welcome){
            this.options.welcome.split("&").forEach(msg => {
                    renderBotMessage({text: msg});
                }
            )
        }

        // Send initial/implicit message by user
        if(this.options.given){
            onSend({text:this.options.given})
        }

        this.info = info;
        this.chitChatClient = chitChatClient;

        // helper functions
        function getSiblings(elem) {
            // Setup siblings array and get the first sibling
            const siblings = [];
            let sibling = elem.parentNode.firstChild;

            // Loop through each sibling and push to the array
            while (sibling) {
                if (sibling.nodeType === 1 && sibling !== elem) {
                    siblings.push(sibling);
                }
                sibling = sibling.nextSibling
            }

            return siblings;
        }


        /**
         * Dynamically load style sheet
         * @param name
         */
        function usestyle(options){
            if(!options.style){
                return;
            }
            const cssId = 'customstyle';  // you could encode the style path itself to generate id..
            if (options.style && !document.getElementById(cssId)){
                const head = document.getElementsByTagName('head')[0];
                const link = document.createElement('link');
                link.id   = cssId;
                link.rel  = 'stylesheet';
                link.type = 'text/css';
                link.href = options.stylePath + 'chitchatclient.style.' + options.style + '.css';
                link.media = 'all';
                head.appendChild(link);
            }
        }

        // load history
        const lastScript = window.localStorage.getItem('script')
        if(this.options.history && this.options.script === lastScript){
            const history = window.localStorage.getItem('history');
            const historyList = history ? JSON.parse(history).filter(m => m !== null) : [];
            for (const msgObj of historyList) {
                switch (msgObj.place) {
                    case "info": this.chitChatClient.addHistoryInfo(msgObj.text); break;
                    case "left": this.chitChatClient.addHistoryMessageLeft(renderBotMessage(msgObj, true), msgObj.user, new Date(JSON.parse('"'+msgObj.timestamp+'"')).toLocaleString(self.options.lang)); break;
                    case "right": this.chitChatClient.addHistoryMessageRight(msgObj.text, msgObj.user, new Date(JSON.parse('"'+msgObj.timestamp+'"')).toLocaleString(self.options.lang)); break;
                }
            }
        } else {
            window.localStorage.removeItem('history');
        }
        window.localStorage.setItem('script', this.options.script)
    }

    coreClient() {
        return this.chitChatClient;
    }

    info() {
        return this.info;
    }

    addWait(delay) {
        this.chitChatClient.addWait(delay)
    }

    loadHistory(){
        if(this.options.history){
            const history = window.localStorage.getItem('history');
            const historyList = history ? JSON.parse(history).filter(m => m !== null) : [];
            for (const msgObj of historyList) {
                switch (msgObj.place) {
                    case "info": this.chitChatClient.addHistoryInfo(msgObj.text); break;
                    case "left": this.chitChatClient.addHistoryMessageLeft(msgObj.text, msg.user, new Date(JSON.parse(msgObj.timestamp))); break;
                    case "right": this.chitChatClient.addHistoryMessageRight(msgObj.text, msg.user, new Date(JSON.parse(msgObj.timestamp))); break;
                }
            }
        }
    }

    updateHistory(msgObj){
        if(this.options.history){
            const history = window.localStorage.getItem('history');
            const historyList = history ? JSON.parse(history).filter(m => m !== null) : [];
            historyList.push(msgObj);
            window.localStorage.setItem('history', JSON.stringify(historyList));
        }
    }

    addMessageLeft(msg, user, delay) {
        const msgObj = this.chitChatClient.addMessageLeft(msg, user, delay);
        this.updateHistory(msgObj)
    }

    /*addMessageRight(msg, user, delay) {
        const msgObj = this.chitChatClient.addMessageRight(msg, user, delay);
        this.updateHistory(msgObj)
    }*/

    addInfo(msg) {
        const msgObj = this.chitChatClient.addInfo(msg);
        this.updateHistory(msgObj)
    }

    addHistoryMessageLeft(msg, user, timestr) {
        this.chitChatClient.addHistoryMessageLeft(msg, user, timestr)
    }

    addHistoryMessageRight(msg, user, timestr) {
        this.chitChatClient.addHistoryMessageRight(msg, user, timestr)
    }

    addHistoryInfo(msg) {
        this.chitChatClient.addHistoryInfo(msg)
    }

    clear(){
        // clear UI
        this.chitChatClient.clear()
        // clear history
        window.localStorage.removeItem('history')
        // reset conversation
        let url;
        if(this.options.chitChatEndpoint){
            url = this.options.chitChatEndpoint.replace('/reply', '/conversation/reset'
                + '/'+encodeURIComponent(this.options.userId)
                + '/'+encodeURIComponent(this.options.botId))
        } else if(this.options.script){
            url = this.options.chitChatHost
                + '/api/v1/channel/chitchat/mt/'
                + encodeURIComponent(this.options.script)
                + '/conversation/reset'
                + '/'+encodeURIComponent(this.options.userId)
                + '/'+encodeURIComponent(this.options.botId)
            ;
        }

        const xhr = new XMLHttpRequest();
        xhr.open('DELETE', url);
        xhr.onload = function() {
            if (xhr.status !== 200) {
                console.error('Could not reset the conversation with: ' + url)
            }
        };
        xhr.send();
    }

    onSend(f){
        this.chitChatClient.options.onSend = f
    }

}



