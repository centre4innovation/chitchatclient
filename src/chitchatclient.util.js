/**
 * Utility functions used for the chat client.
 * @author Arvid Halma, Center for Innovation, Leiden University
 */

/**
 * Update the given object with all key-value pairs from query params.
 * @param options map (Object) to be updated
 */
function updateWithQueryParams(options){
    // get the query string without the ?
    const qs = location.search.substring(1);
    // make a regex pattern to grab key/value
    const pattern = /([^&=]+)=([^&]*)/g;
    // loop the items in the query string, either
    // find a match to the argument, or build an object
    // with key/value pairs
    let itm;
    while (itm = pattern.exec(qs)) {
        options[decodeURIComponent(itm[1])] = decodeURIComponent(itm[2]);
    }
}

function getQueryString() {
    let key = false, res = {}, itm = null;
    // get the query string without the ?
    const qs = location.search.substring(1);
    // check for the key as an argument
    if (arguments.length > 0 && arguments[0].length > 1)
        key = arguments[0];
    // make a regex pattern to grab key/value
    const pattern = /([^&=]+)=([^&]*)/g;
    // loop the items in the query string, either
    // find a match to the argument, or build an object
    // with key/value pairs
    while (itm = pattern.exec(qs)) {
        if (key !== false && decodeURIComponent(itm[1]) === key)
            return decodeURIComponent(itm[2]);
        else if (key === false)
            res[decodeURIComponent(itm[1])] = decodeURIComponent(itm[2]);
    }

    return key === false ? res : null;
}

function randomString(len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let randomString = '';
    for (let i = 0; i < len; i++) {
        const randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
}


