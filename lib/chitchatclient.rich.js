'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Pure JS chat client widget, with predefined behavior to connect to a ChitChat server.
 * @author Arvid Halma, Center for Innovation, Leiden University
 */
var RichChitChatClient = function () {

    /**
     * Create and show a new rich chat client.
     * @param parent {HTMLElement} container
     * @param options {object} with fields to customize the client. In addition to the core client properties,
     * you can specify:
     * {
            lang: 'en',                  // Language code used for UI texts and speech recognition
            chitChatHost: '',            // ChitChat server base url
            chitChatEndpoint: undefined, // url path to /reply
            title: 'ChitChat',           // Title text
            description: undefined,      // Subtitle text
            userId: 'user-' + randomString(8), // User identifier
            autoSendAfterSpeech: true,   // Need to push send button or not
            script: undefined,           // Chitchat bot identifier
            welcome: undefined,          // Initial message shown
            given: undefined,            // Implicit text send by user at start
            stylePath: '',               // Path prefix for css styles (e.g. '/static')
            style: undefined             // Part of css style file 'green' (refers to 'chitchatclient.style.green.css'
        }
     */
    function RichChitChatClient(parent, options) {
        _classCallCheck(this, RichChitChatClient);

        var self = this;
        // override
        this.options = Object.assign({
            lang: 'en', // Language code used for UI texts and speech recognition
            chitChatHost: '', // ChitChat server base url
            chitChatEndpoint: undefined, // url path to /reply
            title: 'ChitChat', // Title text
            description: undefined, // Subtitle text
            userId: 'user-' + randomString(8), // User identifier
            botId: 'chitchat', // Bot identifier
            autoSendAfterSpeech: true, // Need to push send button or not
            script: undefined, // Chitchat bot identifier
            welcome: undefined, // Initial message shown
            given: undefined, // Implicit text send by user at start
            stylePath: '', // Path prefix for css styles (e.g. '/static')
            style: undefined, // Part of css style file 'green' (refers to 'chitchatclient.style.green.css'
            history: true // Whether messages are stored and recovered from local storage
        }, options);

        updateWithQueryParams(this.options);

        usestyle(this.options);

        var info = {};
        info.userAgent = navigator.userAgent;
        info.lang = this.options.lang;

        // Get language setting
        var lang = this.options.lang;
        function speechLangString(lang) {
            speechLang = 'en-US';
            if (lang) {
                if (lang.length === 2 && lang !== 'en') {
                    speechLang = lang.toLowerCase() + '-' + lang.toUpperCase();
                } else {
                    speechLang = lang;
                }
            }
            return speechLang;
        }

        // Update page title
        var title = this.options.title;
        var titleElt = document.getElementById('chitchatclient-title');
        if (title) {
            document.title = title;
            if (titleElt) {
                titleElt.innerText = title;
                titleElt.style.display = 'inline';
            }
        } else {
            // hide title
            if (titleElt) {
                titleElt.style.display = 'none';
            }
        }

        // Update description
        var description = this.options.description;
        var descriptionElt = document.getElementById('chitchatclient-description');
        if (descriptionElt) {
            if (description) {
                descriptionElt.innerText = description;
                descriptionElt.style.display = 'inline';
            } else {
                // hide title
                descriptionElt.style.display = 'none';
            }
        }

        // Update logo
        var logo = this.options.logo;
        if (logo) {
            var logoElt = document.getElementById('chitchatclient-logo');
            if (logoElt) {
                logoElt.src = this.options.logo;
            }
        }

        // Create core client
        var chitChatClient = new ChitChatClient(parent, this.options);
        this.chitChatClient = chitChatClient;

        document.querySelectorAll('.chitchat-reset').forEach(function (e) {
            return e.addEventListener('click', function () {
                self.clear();
            });
        });

        // Setup speech synthesis
        var speechSynth = void 0;
        var speechLang = void 0;
        if ("speechSynthesis" in window) {
            info.speechSynthesis = 'enabled';
            speechSynth = window.speechSynthesis;
            speechLang = speechLangString(lang);
        } else {
            info.speechSynthesis = 'disabled';
        }

        // Setup speech recognition
        var SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
        var micBtn = parent.querySelector('.micBtn');
        if (SpeechRecognition) {
            info.speechRecognition = 'enabled';
            micBtn.style.display = 'inline-flex';
            var micBtnColor = micBtn.style.backgroundColor;

            var recognition = new SpeechRecognition();
            recognition.lang = speechLangString(lang);

            micBtn.addEventListener('click', function () {
                recognition.start();
                micBtn.style.backgroundColor = '#f66e84';
                recognition.onresult = function (event) {
                    var speechToText = event.results[0][0].transcript;
                    var $input = parent.querySelector('.send input');
                    $input.value = speechToText;
                    micBtn.style.backgroundColor = micBtnColor;

                    if (self.options.autoSendAfterSpeech) {
                        parent.querySelector('.sendBtn').click();
                    }
                };
            });
        } else {
            info.speechRecognition = 'disabled';
            micBtn.style.display = 'none';
        }

        function renderBotMessage(reply, historical) {
            var text = reply.text;

            // render image
            text = text.replace(/\bIMAGE *\((.*?)\)/gm, '<img src="$1" style="display: inline;max-width:100%; width: auto; height: auto;"/>');

            // render buttons
            text = text.replace(/\bBUTTON? *\( *([^)]+) *, *(.*?) *\)/gm, '<button type="button" class="btn btn-success chatBtn" data-value="$2" ' + (historical ? 'disabled' : '') + '>$1</button>');
            text = text.replace(/\bBUTTONW? *\( *([^)]+) *, *(.*?) *\)/gm, '<button type="button" class="btn btn-success chatBtn chatBtnWide" data-value="$2" ' + (historical ? 'disabled' : '') + '>$1</button>');
            text = text.replace(/\bBUTTON? *\( *(.*?) *\)/gm, '<button type="button" class="btn btn-success chatBtn" data-value="$1" ' + (historical ? 'disabled' : '') + '>$1</button>');
            text = text.replace(/\bBUTTONW? *\( *(.*?) *\)/gm, '<button type="button" class="btn btn-success chatBtn chatBtnWide" data-value="$1" ' + (historical ? 'disabled' : '') + '>$1</button>');
            text = text.replace(/\bGEOBUTTON *\( *(.*?) *\)/gm, '<button type="button" class="btn btn-success chatBtn geoBtn" data-value="geoBtn" ' + (historical ? 'disabled' : '') + '>$1</button>');
            var textBefore = text;
            text = text.replace(/\bCHECKBOX *\( *([^)]+) *, *(.*?) *\)/gm, '<label class="checkBoxRow">$1 <input type="checkbox" value="$2" ' + (historical ? 'disabled' : '') + '><span class="checkmark"></span></label>');
            text = text.replace(/\bCHECKBOX *\( *(.*?) *\)/gm, '<label class="checkBoxRow">$1 <input type="checkbox" value="$1" ' + (historical ? 'disabled' : '') + '><span class="checkmark"></span></label>');
            if (textBefore !== text && !historical) {
                // checkboxes detected
                text += '<button type="button" class="btn btn-success checkboxSendBtn">OK</button>';
            }

            // render image
            var yw = document.getElementById('chitchatclient').offsetWidth - 125;
            text = text.replace(/\bYOUTUBE *\((.*?)\)/gm, '<div class="youtube" style="width:' + yw + 'px"><iframe src="https://www.youtube.com/embed/$1?version=3&amp;rel=0&amp;showinfo=0" frameborder="0" allow="encrypted-media" allowfullscreen></iframe></div>');

            // speech synthesis
            var speakPattern = /\bSPEAK *\((.*?)\)/gm;
            if (speechSynth && !historical) {
                // if speech supported
                var utterances = void 0;
                while ((utterances = speakPattern.exec(text)) !== null) {
                    var utterThis = new SpeechSynthesisUtterance(utterances[1]);
                    if (speechLang) {
                        utterThis.lang = speechLang;
                    }
                    speechSynth.speak(utterThis);
                }
            }

            // wait
            if (!historical) {
                var waitPattern = /\bWAIT *\((\d+)\)/gm;
                var waits = void 0;
                while ((waits = waitPattern.exec(text)) !== null) {
                    chitChatClient.addWait(waits[1] * 1);
                }
            }

            // remove special codes
            text = text.replace(/\b(SPEAK|WAIT) *\([^)]*\)/gm, '');

            // add text to panel
            return text;
        }

        var onSend = function onSend(params) {
            if (params.text === 'chitchatclientinfo') {

                if (speechSynth) {
                    info.speechSynthesisLangs = speechSynth.getVoices().map(function (v) {
                        return v.lang;
                    }).join(', ');
                }
                chitChatClient.addMessageLeft('ChitChat client details...', '?');
                chitChatClient.addMessageLeft('User ID: ' + self.options.userId, '?');
                chitChatClient.addMessageLeft('Language code: ' + info.lang, '?');
                chitChatClient.addMessageLeft('Browser/user agent: ' + info.userAgent, '?');
                chitChatClient.addMessageLeft('Speech recognition: ' + info.speechRecognition, '?');
                chitChatClient.addMessageLeft('Speech synthesis: ' + info.speechSynthesis + ': ' + info.speechSynthesisLangs, '?');
                return;
            }
            var formData = new FormData();
            formData.append("msg", JSON.stringify({
                id: '' + Date.now(),
                text: params.text,
                senderId: self.options.userId,
                recipientId: self.options.botId,
                timestamp: params.timestamp
            }));

            var url = void 0;
            if (self.options.chitChatEndpoint) {
                url = self.options.chitChatEndpoint;
            } else if (self.options.script) {
                url = self.options.chitChatHost + "/api/v1/channel/chitchat/mt/" + encodeURIComponent(self.options.script) + "/reply";
            } else {
                url = self.options.chitChatHost + "/api/v1/channel/chitchat/reply";
            }

            var xhr = new XMLHttpRequest();
            xhr.open('POST', url);
            xhr.onload = function () {
                if (xhr.status === 200) {
                    var a = JSON.parse(xhr.responseText);
                    a.replies.forEach(function (r) {
                        return self.addMessageLeft(renderBotMessage(r), self.options.leftUserInitials);
                    });
                } else if (xhr.status !== 200) {
                    chitChatClient.addInfo("<div style='color:#f06;font-style: italic'>The chatbot is not available...</div>");
                }
            };
            xhr.send(formData);
            self.updateHistory({ text: params.text, timestamp: params.timestamp, from: self.options.userId, place: 'right' });
        };

        chitChatClient.onSend(onSend);

        // link button actions
        document.addEventListener('click', function (e) {
            if (e.target && e.target.classList.contains('chatBtn')) {
                var value = e.target.getAttribute('data-value');
                if (value === 'geoBtn') {
                    // share geolocation button
                    if ("geolocation" in navigator) {
                        /* geolocation is available */
                        navigator.geolocation.getCurrentPosition(function (position) {
                            var btnParams = {
                                id: '' + Date.now(),
                                text: 'latitude:' + position.coords.latitude + ' longitude:' + position.coords.longitude,
                                senderId: self.options.userId,
                                recipientId: self.options.botId,
                                timestamp: new Date()
                            };

                            e.target.classList.add('chosen');
                            e.target.setAttribute('disabled', true);
                            getSiblings(e.target).forEach(function (btn) {
                                return btn.setAttribute('disabled', true);
                            });
                            onSend(btnParams);
                        });
                    } else {
                        /* geolocation IS NOT available */
                        chitChatClient.addInfo("<div style='color:#f06;font-style: italic'>The geolocation is not available...</div>");
                        // normal button
                        var btnParams = {
                            id: '' + Date.now(),
                            text: 'unknownlocation',
                            senderId: self.options.userId,
                            recipientId: self.options.botId,
                            timestamp: new Date()
                        };
                        e.target.classList.add('chosen');
                        e.target.setAttribute('disabled', true);
                        getSiblings(e.target).forEach(function (btn) {
                            return btn.setAttribute('disabled', true);
                        });
                        onSend(btnParams);
                    }
                } else {
                    // normal button
                    var _btnParams = {
                        id: '' + Date.now(),
                        text: value,
                        senderId: self.options.userId,
                        recipientId: self.options.botId,
                        timestamp: new Date()
                    };
                    e.target.classList.add('chosen');
                    e.target.setAttribute('disabled', true);
                    getSiblings(e.target).forEach(function (btn) {
                        return btn.setAttribute('disabled', true);
                    });
                    onSend(_btnParams);
                }
            }
        });

        // link button actions
        document.addEventListener('click', function (e) {
            if (e.target && e.target.classList.contains('checkboxSendBtn')) {
                var checkedList = [].concat(_toConsumableArray(e.target.parentNode.querySelectorAll('input:checked')));
                var txt = checkedList.map(function (cb) {
                    return cb.value;
                }).join(", ");
                var msgParams = {
                    id: '' + Date.now(),
                    text: txt,
                    senderId: self.options.userId,
                    recipientId: self.options.botId,
                    timestamp: new Date()

                    // submit button
                };e.target.setAttribute('disabled', true);

                // options
                checkedList.forEach(function (cb) {
                    return cb.classList.add('chosen');
                });
                e.target.parentNode.querySelectorAll('input').forEach(function (cb) {
                    return cb.setAttribute('disabled', true);
                });
                onSend(msgParams);
            }
        });

        // Show initial message
        if (this.options.welcome) {
            this.options.welcome.split("&").forEach(function (msg) {
                renderBotMessage({ text: msg });
            });
        }

        // Send initial/implicit message by user
        if (this.options.given) {
            onSend({ text: this.options.given });
        }

        this.info = info;
        this.chitChatClient = chitChatClient;

        // helper functions
        function getSiblings(elem) {
            // Setup siblings array and get the first sibling
            var siblings = [];
            var sibling = elem.parentNode.firstChild;

            // Loop through each sibling and push to the array
            while (sibling) {
                if (sibling.nodeType === 1 && sibling !== elem) {
                    siblings.push(sibling);
                }
                sibling = sibling.nextSibling;
            }

            return siblings;
        }

        /**
         * Dynamically load style sheet
         * @param name
         */
        function usestyle(options) {
            if (!options.style) {
                return;
            }
            var cssId = 'customstyle'; // you could encode the style path itself to generate id..
            if (options.style && !document.getElementById(cssId)) {
                var head = document.getElementsByTagName('head')[0];
                var link = document.createElement('link');
                link.id = cssId;
                link.rel = 'stylesheet';
                link.type = 'text/css';
                link.href = options.stylePath + 'chitchatclient.style.' + options.style + '.css';
                link.media = 'all';
                head.appendChild(link);
            }
        }

        // load history
        var lastScript = window.localStorage.getItem('script');
        if (this.options.history && this.options.script === lastScript) {
            var history = window.localStorage.getItem('history');
            var historyList = history ? JSON.parse(history).filter(function (m) {
                return m !== null;
            }) : [];
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = historyList[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var msgObj = _step.value;

                    switch (msgObj.place) {
                        case "info":
                            this.chitChatClient.addHistoryInfo(msgObj.text);break;
                        case "left":
                            this.chitChatClient.addHistoryMessageLeft(renderBotMessage(msgObj, true), msgObj.user, new Date(JSON.parse('"' + msgObj.timestamp + '"')).toLocaleString(self.options.lang));break;
                        case "right":
                            this.chitChatClient.addHistoryMessageRight(msgObj.text, msgObj.user, new Date(JSON.parse('"' + msgObj.timestamp + '"')).toLocaleString(self.options.lang));break;
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        } else {
            window.localStorage.removeItem('history');
        }
        window.localStorage.setItem('script', this.options.script);
    }

    _createClass(RichChitChatClient, [{
        key: 'coreClient',
        value: function coreClient() {
            return this.chitChatClient;
        }
    }, {
        key: 'info',
        value: function info() {
            return this.info;
        }
    }, {
        key: 'addWait',
        value: function addWait(delay) {
            this.chitChatClient.addWait(delay);
        }
    }, {
        key: 'loadHistory',
        value: function loadHistory() {
            if (this.options.history) {
                var history = window.localStorage.getItem('history');
                var historyList = history ? JSON.parse(history).filter(function (m) {
                    return m !== null;
                }) : [];
                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                    for (var _iterator2 = historyList[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                        var msgObj = _step2.value;

                        switch (msgObj.place) {
                            case "info":
                                this.chitChatClient.addHistoryInfo(msgObj.text);break;
                            case "left":
                                this.chitChatClient.addHistoryMessageLeft(msgObj.text, msg.user, new Date(JSON.parse(msgObj.timestamp)));break;
                            case "right":
                                this.chitChatClient.addHistoryMessageRight(msgObj.text, msg.user, new Date(JSON.parse(msgObj.timestamp)));break;
                        }
                    }
                } catch (err) {
                    _didIteratorError2 = true;
                    _iteratorError2 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }
                    } finally {
                        if (_didIteratorError2) {
                            throw _iteratorError2;
                        }
                    }
                }
            }
        }
    }, {
        key: 'updateHistory',
        value: function updateHistory(msgObj) {
            if (this.options.history) {
                var history = window.localStorage.getItem('history');
                var historyList = history ? JSON.parse(history).filter(function (m) {
                    return m !== null;
                }) : [];
                historyList.push(msgObj);
                window.localStorage.setItem('history', JSON.stringify(historyList));
            }
        }
    }, {
        key: 'addMessageLeft',
        value: function addMessageLeft(msg, user, delay) {
            var msgObj = this.chitChatClient.addMessageLeft(msg, user, delay);
            this.updateHistory(msgObj);
        }

        /*addMessageRight(msg, user, delay) {
            const msgObj = this.chitChatClient.addMessageRight(msg, user, delay);
            this.updateHistory(msgObj)
        }*/

    }, {
        key: 'addInfo',
        value: function addInfo(msg) {
            var msgObj = this.chitChatClient.addInfo(msg);
            this.updateHistory(msgObj);
        }
    }, {
        key: 'addHistoryMessageLeft',
        value: function addHistoryMessageLeft(msg, user, timestr) {
            this.chitChatClient.addHistoryMessageLeft(msg, user, timestr);
        }
    }, {
        key: 'addHistoryMessageRight',
        value: function addHistoryMessageRight(msg, user, timestr) {
            this.chitChatClient.addHistoryMessageRight(msg, user, timestr);
        }
    }, {
        key: 'addHistoryInfo',
        value: function addHistoryInfo(msg) {
            this.chitChatClient.addHistoryInfo(msg);
        }
    }, {
        key: 'clear',
        value: function clear() {
            // clear UI
            this.chitChatClient.clear();
            // clear history
            window.localStorage.removeItem('history');
            // reset conversation
            var url = void 0;
            if (this.options.chitChatEndpoint) {
                url = this.options.chitChatEndpoint.replace('/reply', '/conversation/reset' + '/' + encodeURIComponent(this.options.userId) + '/' + encodeURIComponent(this.options.botId));
            } else if (this.options.script) {
                url = this.options.chitChatHost + '/api/v1/channel/chitchat/mt/' + encodeURIComponent(this.options.script) + '/conversation/reset' + '/' + encodeURIComponent(this.options.userId) + '/' + encodeURIComponent(this.options.botId);
            }

            var xhr = new XMLHttpRequest();
            xhr.open('DELETE', url);
            xhr.onload = function () {
                if (xhr.status !== 200) {
                    console.error('Could not reset the conversation with: ' + url);
                }
            };
            xhr.send();
        }
    }, {
        key: 'onSend',
        value: function onSend(f) {
            this.chitChatClient.options.onSend = f;
        }
    }]);

    return RichChitChatClient;
}();